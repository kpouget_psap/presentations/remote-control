$(document).ready(function() {
    namespace = '/test';
    var socket = io(namespace);

    socket.on('connect', function() {
        socket.emit('my_event', {data: 'connected to the Viewer SocketServer...'});
    });

    socket.on('my_response', function(msg, cb) {
        if (msg.data == "next") {
            document.getElementById('next').click();
        } else if (msg.data == "prev") {
            document.getElementById('previous').click();
        }
    });
});

/*

    <script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/socket.io/3.1.0/socket.io.js"></script>
    <script type="text/javascript" charset="utf-8"  src="/viewer_control.js"></script>

    <script>
      function logKey(e) {
          if (e.keyCode == 32) { // space
              alert("next")
              document.getElementById('next').click();
              e.preventDefault()
          } else if (e.keyCode == 111) { // o
              alert("prev")
              document.getElementById('previous').click();
              e.preventDefault()
          } else {
              alert("hit")
              alert(e.keyCode)
          }
      }

      document.addEventListener('keypress', logKey);
    </script>

*/
