$(document).ready(function() {

    namespace = 'http://972.ovh:5000/test';
    var socket = io(namespace);

    socket.on('connect', function() {
        socket.emit('my_event', {data: 'connected to the Viewer SocketServer...'});
    });

    socket.on('my_response', function(msg, cb) {
        if (msg.data == "next") {
            $('#log').append('<br>' + $('<div/>').text('logs # NEXT').html());
        } else if (msg.data == "prev") {
            $('#log').append('<br>' + $('<div/>').text('logs # PREV').html());
        } else {
            $('#log').append('<br>' + $('<div/>').text('logs #' + msg.count + ': ' + msg.data).html());
        }
    });

});
